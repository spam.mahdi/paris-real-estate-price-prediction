This repository is a part of a medium article: https://medium.com/@Mahdi-Fadil


# Instructions to launch the back-end 

You have to be on the ModelBackend folder

```
python3 manage.py runserver
```
# Instructions to launch the front-end 

You have to be on the front-end folder

```
yarn start
```

Now you can predict paris appartement price by giving few information about it.

If you want to modify the model we can use the notebook in the BuildModel folder

Here you can find how the web app looks like.

You can predict the price of a specific appartement giving some details 

<img alt="web app screenshot" src="https://gitlab.com/spam.mahdi/paris-real-estate-price-prediction/-/tree/main/screenshots/web_app.PNG?raw=true">

You can also use batch to predict several prices with a csv file.

<img alt="excel upload" src="https://gitlab.com/spam.mahdi/paris-real-estate-price-prediction/-/tree/main/screenshots/web_app.PNG?raw=true">


Enjoy!
