import joblib
import numpy as np
import googlemaps
import json
import requests
import datetime

class prediction:
    """
    prediction
    
    Attributes
    ------
    blabla: str
        blabla
    blabla2: float
        blabla
        
    Methods
    ------
    prediction()
        fait nune prediction
    """

    def __init__(self, nb_piece, surface, adresse, code_postal):
        self.nb_piece = nb_piece
        self.surface = surface
        self.adresse = adresse
        self.dep = str(code_postal)
        self.list_paris = ['75']
        self.list_gv = ['13', '33', '69']
        self.list_mv = ['76']
        self.model_paris = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/rf_paris.pkl'
        self.model_gv = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/rf_gv.pkl'
        self.model_mv = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/rf_mv.pkl'
        self.scaler_paris = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/scaler_paris.pkl'
        self.scaler_gv = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/scaler_gv.pkl'
        self.scaler_mv = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/scaler_mv.pkl'
        self.ohe_arr_path = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/ohe_arrondissement.pkl'
        self.ohe_dep_path = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/ohe_departement.pkl'
        self.ohe_par_path = 'C:/Users/maahh/Desktop/ReactAndDjango/ModelBackend/media/firstPage/modele/doc/ohe_parcelle.pkl'
            
    def _get_parcelle(self, gmaps):
        geocode_result = gmaps.geocode(self.adresse + ', ' + self.dep)
        lat = str(geocode_result[0]['geometry']['location']['lat'])
        lng = str(geocode_result[0]['geometry']['location']['lng'])
        link = 'https://geocodage.ign.fr/look4/parcel/reverse?searchGeom={%22type%22:%22Point%22,%22coordinates%22:[' + lng + ',' + lat + ']}'
        r = requests.get(link)
        parcelle = r.json()['features'][0]['properties']['section']
        return(parcelle)
        
#RAJOTUER EGALEMENT POUR LE FICHIER L'ADRESSE
    def pred(self, parcelle):
        if self.dep[0:2] in self.list_paris:
            model = joblib.load(self.model_paris)
            scaler = joblib.load(self.scaler_paris)
        elif self.dep[0:2] in self.list_gv:
            model = joblib.load(self.model_gv)
            scaler = joblib.load(self.scaler_gv)
        elif self.dep[0:2] in self.list_mv:
            model = joblib.load(self.model_mv)
            scaler = joblib.load(self.scaler_mv)
        ohe_arr = joblib.load(self.ohe_arr_path)
        ohe_dep = joblib.load(self.ohe_dep_path)
        ohe_par = joblib.load(self.ohe_par_path)
        year = datetime.datetime.now().year
        X = scaler.transform(np.array([self.surface, self.nb_piece, year]).reshape(1,-1))
        arr = self.dep[2:5]
        dep = self.dep[0:2]
        dep = ohe_dep.transform(np.array(dep).reshape(-1,1))
        arr = ohe_arr.transform(np.array(arr).reshape(-1,1))
        par = ohe_par.transform(np.array(parcelle).reshape(-1,1))
        row = np.hstack((X, dep, arr, par))
        return model.predict(row)
    
    