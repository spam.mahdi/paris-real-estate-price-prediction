from django.shortcuts import render

from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.

import json
import pandas as pd
from firstPage.modele.connect_api import api_connection
from firstPage.modele.back_end import prediction
from django.core.files.storage import FileSystemStorage


def scoreJson(request):
    input = json.loads(request.body)
    gmaps = api_connection().connect_to_api()
    pred = prediction(input['nb_piece'], input['surface'], input['adresse'], input['dep'])
    parcelle = 'AB'
    price = pred.pred(parcelle)[0]
    return JsonResponse({'price':"{:,.2f}".format(round(price, 2))})


def scoreFile(request):
    fileObj = request.FILES['filePath']
    fs=FileSystemStorage()
    filePathName=fs.save(fileObj.name,fileObj)
    filePathName=fs.url(filePathName)
    filePath='.'+filePathName
    gmaps = api_connection().connect_to_api()
    input = pd.read_csv(filePath, sep='\t')
    list_prediction=[]
    for index, row in input.iterrows():
        pred = prediction(row['nb_piece'], row['surface'], row['adresse'], row['dep'])
        parcelle = 'AB'
        price = pred.pred(parcelle)[0]
        #price = prediction(row['nb_piece'], row['surface'], row['dep'], row['parcelle']).pred()[0]
        list_prediction.append("{:,.2f}".format(round(price, 2)))
    return JsonResponse({"list_price":list_prediction})