import './App.css';
import { Component } from 'react/cjs/react.development';

function Home() {
  return (
    <div className="Home">
    <h1> Rentrez les caractéristiques du bien immobilier</h1>
    <JsonForm/>
    </div>
  );
}

export default Home;


class JsonForm extends Component{
    constructor(props){
        super(props);
        this.state={nb_piece:"", surface:"", adresse:"", dep:""};


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    
    
    }

    handleChange(event){
        // console.log(event.target.name+' '+event.target.value);
        this.setState({[event.target.name]:event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        //this.setState({price:10})
        const url="http://localhost:8000/scoreJson";
        
        const bodyData=JSON.stringify({
            "nb_piece": this.state.nb_piece,
            "surface": this.state.surface,
            "adresse": this.state.adresse,
            "dep": this.state.dep,
        });
        const reqOpt = {method:"POST", headers:{"Content-Type":"application/json"}, body: bodyData};
        fetch(url,reqOpt)
        .then((resp) => resp.json())
        .then((respJ) => this.setState({price:respJ.price}))
    }


    render(){
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                <table>
                    <tbody>

                    <tr><td>nb_piece:</td><td>
                    <input type="text" value={this.state.nb_piece} name="nb_piece" onChange={this.handleChange}></input></td></tr>
                    <tr><td>surface:</td><td>
                    <input type="text" value={this.state.surface} name="surface" onChange={this.handleChange}></input></td></tr>
                    <tr><td>adresse:</td><td>
                    <input type="text" value={this.state.adresse} name="adresse" onChange={this.handleChange}></input></td></tr>
                    <tr><td>departement:</td><td>
                    <input type="text" value={this.state.dep} name="dep" onChange={this.handleChange}></input></td></tr>
                   
                    </tbody>
                </table>
                <input type="submit" value="submit" ></input>
                </form>
                <div><h3>Le prix estimé est de: {this.state.price}</h3></div>
            </div>
        );
    }
}
