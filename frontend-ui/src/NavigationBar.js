import './App.css';
import {Link} from 'react-router-dom';


function Navigator() {
  return (
    <div className="Navigator">
        <nav>
            <h2>Mon Estimateur Immobilier</h2>
            <ul className = 'nvlink'>
                <Link to ="/"><li>Acceuil</li></Link>
                <Link to ="/batchProcessing"><li>Estimation via excel</li></Link>
            </ul>
        </nav>
    </div>
  );
}

export default Navigator;
